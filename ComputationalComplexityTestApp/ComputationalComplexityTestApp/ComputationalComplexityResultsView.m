//
//  ComputationalComplexityResultsView.m
//  ComputationalComplexityTestApp
//
//  Created by Vadim Voytenko on 19.01.16.
//  Copyright © 2016 Vadim Voytenko. All rights reserved.
//

#import "ComputationalComplexityResultsView.h"
#include <stdlib.h>

@interface ComputationalComplexityResultsView ()
@property (weak, nonatomic) IBOutlet UIPickerView *dataStructurePicker;
@property (weak, nonatomic) IBOutlet UIPickerView *operationPicker;
@property (weak, nonatomic) IBOutlet UITextView *resultsTextView;
@property (weak, nonatomic) IBOutlet UITableViewCell *elementCountTextField;
@property (weak, nonatomic) IBOutlet UITextField *countOfOperations;


@property (strong, nonatomic) NSArray *dataStructures;
@property (strong, nonatomic) NSArray *operations;
@property (strong, nonatomic) NSString *baseString;

@end

@implementation ComputationalComplexityResultsView

const int sizeOfTestStructure = 1000;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dataStructurePicker.delegate = self;
    self.dataStructurePicker.dataSource = self;
    
    self.operationPicker.delegate = self;
    self.operationPicker.delegate = self;
    
    self.dataStructures = @[@"NSArray",@"NSMutableArray",@"NSDictionary",@"NSMutableDictionary",@"NSSet",@"NSMutableSet"];
    self.operations = @[@"Get",@"Set"];
    

    
    self.baseString = [NSString stringWithFormat:@"Time of calculating of single random value:%f\n",-[self getTimeOfExecutingOfGeneratingOfSingleRandomValue]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if(pickerView == self.dataStructurePicker) {
        return self.dataStructures.count;
    }
    else {
        return self.operations.count;
    }
    
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if(pickerView == self.dataStructurePicker) {
        return [self.dataStructures objectAtIndex:row];
    }
    else {
        return [self.operations objectAtIndex:row];
    }
}

- (IBAction)calculateButtonClicked:(id)sender {
    NSString *resultOfTest = nil;
    NSInteger selectedRowIndex = [self.dataStructurePicker selectedRowInComponent:0];
    NSString* selectedDataStructure = [self.dataStructures objectAtIndex:selectedRowIndex];
    
    selectedRowIndex = [self.operationPicker selectedRowInComponent:0];
    NSString* selectedOperation = [self.operations objectAtIndex:selectedRowIndex];
    
    NSInteger countOfOperations = [self.countOfOperations.text integerValue];
    
    if([selectedDataStructure isEqualToString:@"NSArray"]) {
        //init
        NSMutableArray* mutableArray = [[NSMutableArray alloc] init];
        for(int i = 0;i < sizeOfTestStructure; ++i) {
            [mutableArray addObject:[NSString stringWithFormat:@"%d",i]];
        }
        
        NSArray* array = [NSArray arrayWithArray:mutableArray];
        
        
        if([selectedOperation isEqualToString:@"Get"]) {


            resultOfTest = [self executeGetOperationsForArray:array operationsCount:countOfOperations];
//            self.resultsTextView.text = resultOfTest;
            
        } else if([selectedOperation isEqualToString:@"Set"]) {
            resultOfTest = @"Operation is not available";
        }
    }
    else if([selectedDataStructure isEqualToString:@"NSMutableArray"]) {
        //init
        NSMutableArray* mutableArray = [[NSMutableArray alloc] init];
        for(int i = 0;i < sizeOfTestStructure; ++i) {
            [mutableArray addObject:[NSString stringWithFormat:@"%d",i]];
        }
        
        if([selectedOperation isEqualToString:@"Get"]) {
            resultOfTest = [self executeGetOperationsForArray:mutableArray operationsCount:countOfOperations];
        } else if([selectedOperation isEqualToString:@"Set"]) {
            resultOfTest = [self executeSetOperationsForMutableArray:mutableArray operationsCount:countOfOperations];
        }

    }
    else if([selectedDataStructure isEqualToString:@"NSDictionary"]) {
        
        NSMutableDictionary *mutableDictionary = [[NSMutableDictionary alloc] init];
        NSDictionary *dictionary = [NSDictionary dictionaryWithDictionary:mutableDictionary];
        
        if([selectedOperation isEqualToString:@"Get"]) {
            for(int i = 0;i < sizeOfTestStructure; ++i) {
                [mutableDictionary setObject:@(i) forKey:[NSString stringWithFormat:@"%d",i]];
            }
            resultOfTest = [self executeGetOperationsForDictionary:dictionary operationsCount:countOfOperations];
        } else if([selectedOperation isEqualToString:@"Set"]) {
            resultOfTest = @"Operation is not available";
        }
    }
    else if([selectedDataStructure isEqualToString:@"NSMutableDictionary"]) {
        
        NSMutableDictionary *mutableDictionary = [[NSMutableDictionary alloc] init];
        
        if([selectedOperation isEqualToString:@"Get"]) {
            
            for(int i = 0;i < sizeOfTestStructure; ++i) {
                [mutableDictionary setObject:@(i) forKey:[NSString stringWithFormat:@"%d",i]];
            }
            resultOfTest = [self executeGetOperationsForDictionary:mutableDictionary operationsCount:countOfOperations];
        } else if([selectedOperation isEqualToString:@"Set"]) {
            resultOfTest = [self executeSetOperationsForMutableDictionary:mutableDictionary operationsCount:countOfOperations];
        }
    }
    else if([selectedDataStructure isEqualToString:@"NSSet"]) {
        NSMutableSet *mutableSet = [[NSMutableSet alloc] init];
        
        if([selectedOperation isEqualToString:@"Get"]) {
            for(int i = 0;i < sizeOfTestStructure; ++i) {
                [mutableSet addObject:[NSString stringWithFormat:@"%d",i]];
            }
            NSSet *set = [NSSet setWithSet:mutableSet];
            
            resultOfTest = [self executeGetOperationsForSet:set operationsCount:countOfOperations];
        } else if([selectedOperation isEqualToString:@"Set"]) {
            resultOfTest = @"Operation is not available";
        }
    }
    else {
        
        NSMutableSet *mutableSet = [[NSMutableSet alloc] init];
        
        if([selectedOperation isEqualToString:@"Get"]) {
            
            
            if([selectedOperation isEqualToString:@"Get"]) {
                for(int i = 0;i < sizeOfTestStructure; ++i) {
                    [mutableSet addObject:[NSString stringWithFormat:@"%d",i]];
                }
                
                resultOfTest = [self executeGetOperationsForSet:mutableSet operationsCount:countOfOperations];
            } else if([selectedOperation isEqualToString:@"Set"]) {
                resultOfTest = @"Operation is not available";
            }

        } else if([selectedOperation isEqualToString:@"Set"]) {
            resultOfTest = [self executeSetOperationsForMutableSet:mutableSet operationsCount:countOfOperations];
        }
    }
    self.resultsTextView.text = resultOfTest;
}

- (NSString*)executeGetOperationsForArray:(NSArray*) array operationsCount:(NSInteger) operationsCount {
    int randomIndex = 0;
    NSDate *operationStart = [NSDate date];
    for(int i = 0; i < operationsCount; ++i) {
        randomIndex = arc4random_uniform((u_int32_t)array.count);
        NSObject *object = [array objectAtIndex:randomIndex];
    }
    NSTimeInterval timeIntervalOfOperation = [operationStart timeIntervalSinceNow];
    
    NSString *timeOfPerformingOfOperation = [NSString stringWithFormat:@"%f",-timeIntervalOfOperation];
    return [NSString stringWithFormat:@"%@Time of executing of get operations:%@ sec\nAvarange time of single operation: %f microsec",self.baseString,timeOfPerformingOfOperation,-timeIntervalOfOperation/operationsCount];
}

- (NSString*)executeSetOperationsForMutableArray:(NSMutableArray*) array operationsCount:(NSInteger) operationsCount {
    NSDate *operationStart = [NSDate date];
    for(int i = 0;i < operationsCount; ++i) {
        [array addObject:[NSString stringWithFormat:@"%d",i]];
    }
    NSTimeInterval timeIntervalOfOperation = [operationStart timeIntervalSinceNow];
    
    NSString *timeOfPerformingOfOperation = [NSString stringWithFormat:@"%f",-timeIntervalOfOperation];
    return [NSString stringWithFormat:@"%@Time of executing of set operations:%@ sec\nAvarange time of single operation: %f microsec",self.baseString,timeOfPerformingOfOperation,-timeIntervalOfOperation/operationsCount];
}

- (NSTimeInterval) getTimeOfExecutingOfGeneratingOfSingleRandomValue {
    NSDate *start = [NSDate date];
    int randomIndex = arc4random_uniform(sizeOfTestStructure);
    return [start timeIntervalSinceNow];
}

- (NSString*) executeSetOperationsForMutableDictionary:(NSMutableDictionary*) dictionary operationsCount:(NSInteger) operationsCount {
    NSDate *operationStart = [NSDate date];
    for(int i = 0;i < operationsCount; ++i) {
        [dictionary setObject:@(i) forKey:[NSString stringWithFormat:@"%d",i]];
    }
    NSTimeInterval timeIntervalOfOperation = [operationStart timeIntervalSinceNow];
    
    NSString *timeOfPerformingOfOperation = [NSString stringWithFormat:@"%f",-timeIntervalOfOperation];
    return [NSString stringWithFormat:@"%@Time of executing of set operations:%@ sec\nAvarange time of single operation: %f microsec",self.baseString,timeOfPerformingOfOperation,-timeIntervalOfOperation/operationsCount * pow(10, 6)];
}

- (NSString*) executeGetOperationsForDictionary:(NSDictionary*) dictionary operationsCount:(NSInteger) operationsCount {
    int randomIndex = 0;
    NSDate *operationStart = [NSDate date];
    for(int i = 0; i < operationsCount; ++i) {
        randomIndex = arc4random_uniform((u_int32_t)dictionary.count);
        NSObject *object = [dictionary objectForKey:[NSString stringWithFormat:@"%d",i]];
    }
    NSTimeInterval timeIntervalOfOperation = [operationStart timeIntervalSinceNow];
    
    NSString *timeOfPerformingOfOperation = [NSString stringWithFormat:@"%f",-timeIntervalOfOperation];
    return [NSString stringWithFormat:@"%@Time of executing of get operations:%@ sec\nAvarange time of single operation: %f microsec",self.baseString,timeOfPerformingOfOperation,-timeIntervalOfOperation/operationsCount * pow(10, 6)];
}


- (NSString*) executeSetOperationsForMutableSet:(NSMutableSet*) set operationsCount:(NSInteger) operationsCount {
    NSDate *operationStart = [NSDate date];
    for(int i = 0;i < operationsCount; ++i) {
        [set addObject:[NSString stringWithFormat:@"%d",i]];
    }
    NSTimeInterval timeIntervalOfOperation = [operationStart timeIntervalSinceNow];
    
    NSString *timeOfPerformingOfOperation = [NSString stringWithFormat:@"%f",-timeIntervalOfOperation];
    return [NSString stringWithFormat:@"%@Time of executing of set operations:%@ sec\nAvarange time of single operation: %f microsec",self.baseString,timeOfPerformingOfOperation,-timeIntervalOfOperation/operationsCount * pow(10, 6)];
}

- (NSString*) executeGetOperationsForSet:(NSSet*) set operationsCount:(NSInteger) operationsCount {
    int randomIndex = 0;
    NSDate *operationStart = [NSDate date];
    for(int i = 0; i < operationsCount; ++i) {
        randomIndex = arc4random_uniform((u_int32_t)set.count);
        NSObject *object  = [set anyObject];
    }
    NSTimeInterval timeIntervalOfOperation = [operationStart timeIntervalSinceNow];
    
    NSString *timeOfPerformingOfOperation = [NSString stringWithFormat:@"%f",-timeIntervalOfOperation];
    return [NSString stringWithFormat:@"%@Time of executing of get operations:%@ sec\nAvarange time of single operation: %f microsec",self.baseString,timeOfPerformingOfOperation,-timeIntervalOfOperation/operationsCount * pow(10, 6)];
}

@end
