//
//  ComputationalComplexityResultsView.h
//  ComputationalComplexityTestApp
//
//  Created by Vadim Voytenko on 19.01.16.
//  Copyright © 2016 Vadim Voytenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ComputationalComplexityResultsView : UITableViewController <UIPickerViewDataSource, UIPickerViewDelegate>

@end
